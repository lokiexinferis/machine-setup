#!/bin/bash

WHOAMI=$(whoami)

if [ $WHOAMI == root ]
then
    echo 'This script may not be run as root (or sudo). Please run again'
    echo 'as just your normal user. It will ask for your sudo password itself'
    exit 1
fi

SOURCE="${BASH_SOURCE[0]}"
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

$DIR/shell-setup.sh

# These are intentionally copied and not symlink'd, because they may be
# customized for a particular machine. If they were symlinked, then the git repo
# would get potentially dirty. Maybe I'll revisit this question in the future...
cp $DIR/.tmux.conf ~/.tmux.conf
cp $DIR/.zshrc ~/.zshrc
sed -i "s/CURRENT_USER/$WHOAMI/g" ~/.zshrc
$DIR/vim-setup.sh

echo ''
echo '---------------------------------------------------------------'
echo 'System fonts have been installed. However, to use them you'
echo 'will need to configure your terminal to use the appropriate'
echo 'font. I have been using'
echo '"SourceCodePro+Powerline+Awesome+Regular" at size 11.'
echo "Go to Terminal's Edit-\>Profile Preferences and set the"
echo 'Custom Font as you see fit.'
echo '---------------------------------------------------------------'
echo ''
echo '---------------------------------------------------------------'
echo 'System is now configured. You must reboot in order for some'
echo 'terminal settings (such as login shell) to properly take'
echo 'effect. Please do so now.'
echo '---------------------------------------------------------------'
echo ''
