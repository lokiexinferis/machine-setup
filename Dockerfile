FROM ubuntu:16.04

RUN apt-get -y update && apt-get -y install vim-nox git sudo

RUN useradd -m ilya
RUN adduser ilya sudo
RUN echo 'ilya:pass' | chpasswd
USER ilya

WORKDIR /home/ilya/script
