#!/bin/bash
set -e

VIM_REPO=https://gitlab.com/lokiexinferis/vim-configs.git
# Prerequisits:
# * Powerline fonts, and a 256 color terminal.
#   Note: This may be problematic for some Windows terminal emulators.

pushd ~ > /dev/null
git clone $VIM_REPO ~/.vim
ln -s ~/.vim/vimrc ~/.vimrc
ln -s ~/.vim/vim_runtime ~/.vim_runtime
ln -s ~/.vim/vim_runtime/sources_non_forked/colorschemes/colors ~/.vim/colors

cd .vim
git submodule update --init --recursive

if [ -d ~/.vim/vim_runtime/sources_non_forked/command-t/ruby/command-t ]
then
    sudo apt-get -y install ruby ruby-dev build-essential

    pushd ~/.vim/vim_runtime/sources_non_forked/command-t/ruby/command-t > /dev/null
    ruby extconf.rb
    make
    popd
fi

if [ -d ~/.vim/bundle/YouCompleteMe ]
then
    echo ""
    echo ""
    echo "YouCompleteMe is included with this installation."
    echo "It is a relatively large package which requires a clang compiler be"
    echo "configured, and is not advised for smaller machines."
    echo "Would you like to use it? [Y/n]"
    read -n 1 KEEP_YCM

    if [ "${KEEP_YCM}" == "" ] || \
       [ "${KEEP_YCM}" == "Y" ] || \
       [ "${KEEP_YCM}" == "y" ]
    then
        sudo apt-get -y install cmake build-essential

        pushd ~/.vim/bundle/YouCompleteMe > /dev/null
        ./install.py --clang-completer
        popd
    else
        rm -rf ~/.vim/bundle/YouCompleteMe
    fi
fi
popd
