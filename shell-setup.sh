#!/bin/bash

sudo apt-get -y upgrade
sudo apt-get -y install \
    vim \
    vim-nox \
    zsh \
    git \
    tmux \
    htop \
    powerline \
    wget \
    curl \
    autojump \
    universal-ctags \
    ripgrep \
    dconf-cli

wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh

WHOAMI=$(whoami)
sudo chsh -s `which zsh` $WHOAMI

wget https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf -O /tmp/fonts
git clone https://github.com/gabrielelana/awesome-terminal-fonts.git /tmp/awesome-terminal-fonts

mkdir -p ~/.fonts
cp /tmp/fonts/PowerlineSymbols.otf ~/.fonts/
cp -f /tmp/awesome-terminal-fonts/build/*.ttf ~/.fonts/
cp -f /tmp/awesome-terminal-fonts/build/*.sh ~/.fonts/
git --git-dir=/tmp/awesome-terminal-fonts/.git --work-tree=/tmp/awesome-terminal-fonts checkout patching-strategy
cp -f /tmp/awesome-terminal-fonts/patched/*.ttf ~/.fonts/
cp -f /tmp/awesome-terminal-fonts/patched/*.sh ~/.fonts/

mkdir -p ~/.config/fontconfig/conf.d
cp 10-symbols.conf ~/.config/fontconfig/conf.d/
fc-cache -vf ~/.fonts/

git clone \
    https://github.com/Anthony25/gnome-terminal-colors-solarized.git \
    ~/.terminal-colors
pushd ~/.terminal-colors > /dev/null
wget https://raw.githubusercontent.com/seebi/dircolors-solarized/master/dircolors.256dark
./install.sh
popd
